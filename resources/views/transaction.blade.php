@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10">
        <h4 class="page-header">EGCO427: DBProject</h4>
    </div>
    <div class="row">
        <div class="col-md-10">

		            <div class="row">
		                <div class="col-lg-12">
		                    <div class="panel panel-info">
		                        <div class="panel-heading">
		                            Transaction (using http://mycourse.atilal.com/2013/egci423/dbproject/xml/transactions.php?uid=XX)
		                        </div>
		                        <!-- /.panel-heading -->
		                        <div class="panel-body">
		                            <div class="dataTable_wrapper">
		                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                                    <thead>
		                                        <tr>
		                                            <th>Transno</th>
		                                            <th>User ID</th>
		                                            <th>Date</th>
		                                            <th>Seller No</th>
		                                            <th>Product</th>
		                                            <th>Price</th>
		                                            <th>Number</th>
		                                        </tr>
		                                    </thead>
		                                    <tbody>
		                                    	@foreach($transaction as $item)
		                                        	<tr class="odd gradeX">
			                                            <td>{{$item['transno']}}</td>
			                                            <td>{{$item['uid']}}</td>
			                                            <td>{{$item['date']}}</td>
			                                            <td>{{$item['sellerno']}}</td>
			                                            <td>{{$item['product']}}</td>
			                                            <td>{{$item['price']}}</td>
			                                            <td>{{$item['number']}}</td>
		                                        	</tr> 
		                                        @endforeach
		                                    </tbody>
		                                </table>
		                            </div>
		                            <!-- /.table-responsive -->
		                            
		                        </div>
		                        <!-- /.panel-body -->
		                    </div>
		                    <!-- /.panel -->
		                </div>
		                <!-- /.col-lg-12 -->    
        </div>
    </div>
</div>
@endsection


