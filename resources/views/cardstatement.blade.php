@extends('layouts.app')

@section('content')

   


<div class="container">
    <div class="col-md-10">
        <h4 class="page-header">EGCO427: DBProject</h4>
         @if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{ Session::get('flash_message') }}
            </div>
        @endif

    </div>
    <div class="row">
        <div class="col-md-10">

		            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Credit Card Transaction
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form role="form" id="update-cardstatement" name="card" action="/cardstatement" method="POST">
                                        
                                        <div class="form-group">
                                            <label>Transaction No.</label>
                                            <input  name="id" class="form-control" placeholder="Enter text" readonly value="{{$show->id}}">
                                        </div>

										<label>User ID No.</label>
                                        <div class="form-group input-group">
											<span class="input-group-addon">@</span>
                                            <input name="uid" type="text" class="form-control" placeholder="User ####" readonly value="{{$show->uid}}">
                                        </div>

										<div class="form-group">
                                            <label>Credit Card No.</label>
                                            <input class="form-control" placeholder="Enter Creditcard No." readonly  value="{{$show->number}}">
                                        </div>

										<div class="form-group">
                                            <label>Date</label>
                                            <input type="date" class="form-control" placeholder="Enter Date" readonly value="{{$show->date}}">
                                        </div>
                                    
										<div class="form-group">
                                            <label>Seller No.</label>
                                            <input id="sellerno" name="sellerno" class="form-control" placeholder="Enter Seller No." readonly  value="{{$show->sellerno}}">
                                        </div>

										<div class="form-group">
                                            <label>Product</label>
                                            <input id="product" name="product" class="form-control" placeholder="Enter Product" readonly  value="{{$show->product}}">
                                        </div>

                                        <label>Price</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">$</span>
                                            <input id="price" name="price" type="text" class="form-control" readonly  value="{{$show->price}}">
                                            <span class="input-group-addon">.00</span>
                                        </div>
										<table width='100%'><tr>
                                        <td width ='25%'><button type="button" class="btn btn-default btn-circle btn-lg"  onClick="location.href='{{ URL::to( 'cardstatement/' . $prevTransaction ) }}'"><i class="fa fa-angle-left" ></i></button></td>
										
										<td width ='50%'>	
												<button type="button" class="btn btn-primary btn-circle btn-lg" onClick="location.href='{{ url('/editstatement',['id'=>$show->id]) }}'"><i class="fa fa-edit"></i></button>
												
												<button type="button" class="btn btn-warning btn-circle btn-lg" onClick="location.href='{{ url('/createtransaction')}}'" ><i class="fa fa-plus"></i></button>
                                                
                                                

                                                <form action="/cardstatement/{{ $show->id}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-cardstatement-{{ $show->id}}" class="btn btn-danger btn-circle btn-lg">
                                                    <i class="fa fa-btn fa-trash"></i>
                                                </button>
                                            </form>
                                               


										</td>
										<td  width ='25%'><button type="button" class="btn btn-default btn-circle btn-lg" onClick="location.href='{{ URL::to( 'cardstatement/' . $nextTransaction ) }}'"><i class="fa fa-angle-right"></i></button></td>
										</tr></table>
							
                                    </form>

									</div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>
</div>
@endsection


