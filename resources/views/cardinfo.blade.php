@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10">
        <h4 class="page-header">EGCO427: DBProject</h4>
    </div>
    <div class="row">
        <div class="col-md-10">

		            <div class="row">
		                <div class="col-lg-12">
		                    <div class="panel panel-info">
		                        <div class="panel-heading">
		                            Cardinfo
		                        </div>
		                        <!-- /.panel-heading -->
		                        <div class="panel-body">
		                            <div class="dataTable_wrapper">
		                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                                    <thead>
		                                        <tr>
		                                            <th>Name</th>
		                                            <th>Number</th>
		                                            <th>Issuer</th>
		                                            <th>Exp</th>
		                                            <th>Limit</th>
		                                            <th>Currency</th>
		                                        </tr>
		                                    </thead>
		                                    <tbody>
		                                    	@foreach ($cardinfo as $thiscardinfo)
		                                        	<tr class="odd gradeX">
		                                        	
			                                            <td>{{$thiscardinfo->name}}</td>
			                                            <td>{{$thiscardinfo->number}}</td>
			                                            <td>{{$thiscardinfo->issuer}}</td>
			                                            <td>{{$thiscardinfo->exp}}</td>
			                                            <td>{{$thiscardinfo->limit}}</td>
			                                            <td>{{$thiscardinfo->currency}}</td>
		                                        	</tr>
		                                        @endforeach  
		                                    </tbody>
		                                </table>
		                            </div>
		                            <!-- /.table-responsive -->
		                            
		                        </div>
		                        <!-- /.panel-body -->
		                    </div>
		                    <!-- /.panel -->
		                </div>
		                <!-- /.col-lg-12 -->    
        </div>
    </div>
</div>
@endsection


