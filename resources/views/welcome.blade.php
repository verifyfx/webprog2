@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10">
        <h4 class="page-header">EGCO427: DBProject</h4>
    </div>
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-info">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Your Application's Landing Page.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
