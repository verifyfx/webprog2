@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10">
        <h4 class="page-header">EGCO427: DBProject</h4>
    </div>
    <div class="row">
        <div class="col-md-10">

		            <div class="row">
		                <div class="col-lg-12">
		                    <div class="panel panel-info">
		                        <div class="panel-heading">
		                            Person Information
		                        </div>
		                        <!-- /.panel-heading -->
		                       <div class="panel-body">
	                            <ul class="chat">
	                            		@foreach($personinfo as $person)
	                                <li class="left clearfix">
	                                    <span class="chat-img pull-left">
	                                        <img src="images/user.png" alt="User Avatar" class="img-circle" />
	                                    </span>

	                                    <div class="chat-body clearfix">
                                        <table border="0"><tbody>
                                            <tr>
                                            <td><b>Name:</b></td>
                                            <td>{{$person->firstname}} {{$person->lastname}}</td>
                                            </tr>
                                            <tr>
                                            <td><b>Location:</b></td>
                                            <td>{{$person->city}}</td>
                                            </tr>
                                            <tr>
                                            <td><b>Telephone:</b></td>
                                            <td>{{$person->telephone}}</td>
                                            </tr>
                                            <tr>
                                            <td><b>E-mail</b></td>
                                            <td>{{$person->email}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
	                                    </div>
	                                </li>
	                                @endforeach
	                            </ul>
	                        </div>
		                    </div>
		                    <!-- /.panel -->
		                </div>
		                <!-- /.col-lg-12 -->    
        </div>
    </div>
</div>
@endsection


