@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-10">
        <h4 class="page-header">EGCO427: DBProject</h4>
    </div>
    <div class="row">
        <div class="col-md-10">

	        <div class="row">
	            <div class="col-lg-12">
	                <div class="panel panel-info">
	                    <div class="panel-heading">
	                        Search News
	                    </div>
	                    <!-- /.panel-heading -->
	                    <div class="panel-body">

						<form>
	                      <select name="type" onchange = "javascript:location.href = '/searchnews/'+this.value;" autocomplete="off">
	                        	<option value="1" @if(basename(url()->current())=='1') selected @endif>Sanook News</option>
	                        	<option value="2" @if(basename(url()->current())=='2') selected @endif>Sanook IT News</option>
	                        </select>
	                    </form>
 	                    	<div class="header">
	                    		<h1><a href="{{ $permalink }}">{{ $title }}</a></h1>
	                    	</div>

	                    	@foreach ($items as $item)
	                    	<div class="item">
	                    		<h2><a href="{{ $item->get_permalink() }}">{{ $item->get_title() }}</a></h2>
	                    		<p>{!! $item->get_description() !!}</p>

	                    		<p><small>Posted on {{ $item->get_date('j F Y | g:i a') }}</small></p>
	                    	</div>
	                    	@endforeach

	                    </div>
	                    <!-- /.panel-body -->
	                </div>
	                <!-- /.panel -->
	            </div>
	            <!-- /.col-lg-12 -->    
        </div>
    </div>
</div>
@endsection