<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Cardstatement;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all of the tasks for the user.
     */
    // public function tasks()
    // {
    //     return $this->hasMany(Task::class);
    // }

    public function getId()
    {
      return $this->id;
    }
}
