<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => 'web'],function(){
	Route::auth();
	Route::get('/', function () {
	    return view('welcome');
	});

	Route::get('access', function(){
    	echo 'You have access';
    })->middleware('isAdmin');

	Route::get('personinfo', 'PersoninfoController@personinfo')->middleware('auth');

	Route::get('searchnews', 'NewsController@index')->middleware('auth');
	Route::get('searchnews/{id}', 'NewsController@show')->middleware('auth');

    Route::get('cardinfo', 'CardinfoController@cardinfo')->middleware('auth');
    
    Route::get('transaction', 'TransactionController@index')->middleware('auth');

	Route::get('/home', 'HomeController@index');
});







// Route::auth();

// Route::get('/home', 'HomeController@index');


