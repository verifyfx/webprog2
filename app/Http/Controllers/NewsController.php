<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Feeds;
use View;

class NewsController extends Controller
{
    //
    public function index() {

      $feed = Feeds::make('http://rssfeeds.sanook.com/rss/feeds/sanook/news.index.xml');

    $data = array(
      'title'     => $feed->get_title(),
      'permalink' => $feed->get_permalink(),
      'items'     => $feed->get_items(),
    );

    return view('searchnews', $data);

  }

  public function show($id) {

    if($id == '1') {
      $feed = Feeds::make('http://rssfeeds.sanook.com/rss/feeds/sanook/news.index.xml');
    } else if ($id == '2') { 
      $feed = Feeds::make('http://rssfeeds.sanook.com/rss/feeds/sanook/hitech.news.xml');
    }

    $data = array(
      'title'     => $feed->get_title(),
      'permalink' => $feed->get_permalink(),
      'items'     => $feed->get_items(),
    );
    return view('searchnews', $data);
  }
}
