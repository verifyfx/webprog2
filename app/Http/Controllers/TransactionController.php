<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;


class TransactionController extends Controller
{
    //
    public function index(){
    	$uid = Auth::user()->getId();
        //get xml
        $xmlURL = 'http://mycourse.atilal.com/2013/egci423/dbproject/xml/transactions.php?uid='.$uid;
        
        $xml = simplexml_load_file($xmlURL);
        foreach($xml->children()->children() as $state) {
            $states[] = array('transno' => (string)$state->transno,
                                'uid' => (string)$state->uid,
                                'date' => (string)$state->date,
                                'sellerno' => (string)$state->sellerno,
                                'product' => (string)$state->product,
                                'price' => (string)$state->price,
                                'number' => (string)$state->number);
        }
        return view('Transaction', array('transaction' => $states));
    }
}
