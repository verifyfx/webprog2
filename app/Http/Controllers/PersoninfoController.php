<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use App\Personinfo as Personinfo;

class PersoninfoController extends Controller
{
    //
    public function personinfo(){
    	$uid = Auth::user()->getId();
    	if($uid == 1){
	    	$personinfo = Personinfo::all();
	    }
	    else{
	    	$personinfo = Personinfo::where('id', '=', Auth::user()->getId())->get();
	    }
    	return view('Personinfo', array('personinfo' => $personinfo ));
    }
}
