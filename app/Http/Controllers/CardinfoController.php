<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cardinfo as Cardinfo;
use Auth;

class CardinfoController extends Controller
{
    //
    public function cardinfo(){
    	$uid = Auth::user()->getId();
    	if($uid == 1){
	    	$cardinfo = Cardinfo::all();
	    }
	    else{
	    	$cardinfo = Cardinfo::where('uid', '=', Auth::user()->getId())->get();
	    }
    	return view('Cardinfo', array('cardinfo' => $cardinfo ));
    }
}
